<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */

    private $password;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastscore;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bestscore;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $lastchrono;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $bestchono;

    /**
     * @ORM\Column(type="date")
     */
    private $signindate;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $Avatar;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $mimeType;

    /**
    * @var string The reset token
    * @ORM\Column(type="string")
    */
    private $resetToken;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $GoogleId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $FacebookId;
    public function getResetToken(): string
    {
    return (string) $this->resetToken;
    }
    public function setResetToken(string $resetToken): self
    {
    $this->resetToken = $resetToken;
    return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getLastscore(): ?int
    {
        return $this->lastscore;
    }

    public function setLastscore(?int $lastscore): self
    {
        $this->lastscore = $lastscore;

        return $this;
    }

    public function getBestscore(): ?int
    {
        return $this->bestscore;
    }

    public function setBestscore(?int $bestscore): self
    {
        $this->bestscore = $bestscore;

        return $this;
    }

    public function getLastchrono(): ?\DateTimeInterface
    {
        return $this->lastchrono;
    }

    public function setLastchrono(?\DateTimeInterface $lastchrono): self
    {
        $this->lastchrono = $lastchrono;

        return $this;
    }

    public function getBestchono(): ?\DateTimeInterface
    {
        return $this->bestchono;
    }

    public function setBestchono(?\DateTimeInterface $bestchono): self
    {
        $this->bestchono = $bestchono;

        return $this;
    }

    public function getSignindate(): ?\DateTimeInterface
    {
        return $this->signindate;
    }

    public function setSignindate(\DateTimeInterface $signindate): self
    {
        $this->signindate = $signindate;

        return $this;
    }

    public function getAvatar()
    {
        return stream_get_contents($this->Avatar);
    }

    public function setAvatar($Avatar): self
    {
        $this->Avatar = $Avatar;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getGoogleId(): ?int
    {
        return $this->GoogleId;
    }

    public function setGoogleId(?int $GoogleId): self
    {
        $this->GoogleId = $GoogleId;

        return $this;
    }

    public function getFacebookId(): ?int
    {
        return $this->FacebookId;
    }

    public function setFacebookId(?int $FacebookId): self
    {
        $this->FacebookId = $FacebookId;

        return $this;
    }
}
