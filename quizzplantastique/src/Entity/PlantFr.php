<?php

namespace App\Entity;

use App\Repository\PlantFrRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlantFrRepository::class)
 */
class PlantFr
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $latin_Name;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Native_from;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Genus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Family;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Leaf_color;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Common_use;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Flowerin_in;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Plant_type;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getLatinName(): ?string
    {
        return $this->latin_Name;
    }

    public function setLatinName(string $latin_Name): self
    {
        $this->latin_Name = $latin_Name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getNativeFrom(): ?string
    {
        return $this->Native_from;
    }

    public function setNativeFrom(string $Native_from): self
    {
        $this->Native_from = $Native_from;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getGenus(): ?string
    {
        return $this->Genus;
    }

    public function setGenus(string $Genus): self
    {
        $this->Genus = $Genus;

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->Family;
    }

    public function setFamily(string $Family): self
    {
        $this->Family = $Family;

        return $this;
    }

    public function getLeafColor(): ?string
    {
        return $this->Leaf_color;
    }

    public function setLeafColor(string $Leaf_color): self
    {
        $this->Leaf_color = $Leaf_color;

        return $this;
    }

    public function getCommonUse(): ?string
    {
        return $this->Common_use;
    }

    public function setCommonUse(string $Common_use): self
    {
        $this->Common_use = $Common_use;

        return $this;
    }

    public function getFlowerinIn(): ?string
    {
        return $this->Flowerin_in;
    }

    public function setFlowerinIn(string $Flowerin_in): self
    {
        $this->Flowerin_in = $Flowerin_in;

        return $this;
    }

    public function getPlantType(): ?string
    {
        return $this->Plant_type;
    }

    public function setPlantType(string $Plant_type): self
    {
        $this->Plant_type = $Plant_type;

        return $this;
    }

}
