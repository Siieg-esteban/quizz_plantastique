<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\PlantFr;
use App\Entity\PlantEn;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class PlantType extends AbstractType
{
    private $translator;
    public function __construct(TranslatorInterface $translator){
        $this->translator=$translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $option
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
            ->add('name', TextType::class, array('label'=>'listing.name', 'attr' => array(
                'class' => 'form-control',
                'title' => 'name',
            )))
            ->add('latinname', TextType::class, array('label'=>'listing.latin', 'attr' => array(
                'class' => 'form-control',
                'title' => 'latinname',
            )))
            ->add('description', TextareaType::class, array('label'=>'listing.desc', 'attr' => array(
                'class' => 'form-control',
                'title' => 'description',
            )))
            ->add('native', TextType::class, array('label'=>'listing.native', 'attr' => array(
                'class' => 'form-control',
                'title' => 'native',
            )))
            ->add('type', ChoiceType::class, array('label'=>'choices.type.type', 
                'choices' => array (
                    'choices.con' =>$this->translator->trans('choices.con') ,
                    'choices.broad' => $this->translator->trans('choices.broad'),
                ),
                'attr' => array(
                    'class' => 'form-control',
                    'title' => 'choices.type.type', 
                )
                ))
            ->add('genus', TextType::class, array('label'=>'listing.genus', 'attr' => array(
                'class' => 'form-control',
                'title' => 'genus',
            )))
            ->add('family', TextType::class, array('label'=>'listing.family', 'attr' => array(
                'class' => 'form-control',
                'title' => 'family',
            )))
            ->add('color', TextType::class, array('label'=>'listing.color', 'attr' => array(
                'class' => 'form-control',
                'title' => 'color',
            )))
            ->add('use', TextType::class, array('label'=>'listing.use', 'attr'=> array(
                'class' => 'form-control',
                'title' => 'use',
            )))
            ->add('flower', TextType::class, array('label'=>'listing.flower', 'attr' => array(
                'class' => 'form-control',
                'title' => 'flower',
            )))
            ->add('planttype', ChoiceType::class, array('label'=>'listing.ptype',
            'choices' => array(
                'choices.planttype.tree' => $this->translator->trans('choices.planttype.tree'),
                'choices.planttype.vine' => $this->translator->trans('choices.planttype.vine'),
                'choices.planttype.plant' => $this->translator->trans('choices.planttype.plant')
            ),
            'attr' => array(
                'class' => 'form-control',
                'title' => 'plantype',
            )))
            ->add('image1', FileType::class, [
                'label'=>'listing.image1',
                'mapped'=>false,
                'required'=>true,
                'constraints'=>[
                    new File([
                        'maxSize'=>'1024k',
                        'mimeTypes'=>[
                            'image/png'
                        ],
                        'mimeTypesMessage'=> $this->translator->trans('listing.imageerror')
                    ])
                ]
            ])
            ->add('image2', FileType::class, [
                'label'=>'listing.image2',
                'mapped'=>false,
                'required'=>true,
                'constraints'=>[
                    new File([
                        'maxSize'=>'1024k',
                        'mimeTypes'=>[
                            'image/png'
                        ],
                        'mimeTypesMessage'=> $this->translator->trans('listing.imageerror')
                    ])
                ]
            ])
            ->add('save', SubmitType::class, array(
                'label' => 'listing.save',
                'attr' => array(
                    'class' => 'btn btn-primary btn-margin',
                    'title' => 'Enregistrer'
                )
            ));
    }
}
?>