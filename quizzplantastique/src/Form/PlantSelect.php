<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\PlantFr;
use App\Entity\PlantEn;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Contracts\Translation\TranslatorInterface;

class PlantSelect extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $option
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder
        ->add('lang', ChoiceType::class, array('label'=>'choices.lang.lang', 
            'choices' => array(
                'choices.lang.fr' => 'fr',
                'choices.lang.en' => 'en',
        )))
        ->add('save', SubmitType::class, array(
            'label' => 'listing.save',
            'attr' => array(
                'class' => 'btn btn-primary btn-margin',
                'title' => 'listing.save'
            )
        ));
    }
}



?>