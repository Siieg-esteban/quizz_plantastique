<?php

namespace App\Repository;

use App\Entity\PlantEn;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlantEn|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlantEn|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlantEn[]    findAll()
 * @method PlantEn[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlantEnRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlantEn::class);
    }

    // /**
    //  * @return PlantEn[] Returns an array of PlantEn objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlantEn
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
