<?php

namespace App\Repository;

use App\Entity\PlantFr;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlantFr|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlantFr|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlantFr[]    findAll()
 * @method PlantFr[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlantFrRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlantFr::class);
    }

    // /**
    //  * @return PlantFr[] Returns an array of PlantFr objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlantFr
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
