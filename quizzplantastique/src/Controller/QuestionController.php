<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

use App\Entity\PlantEn;
use App\Entity\PlantFr;
use App\Entity\User;

use ReflectionMethod;

class QuestionController extends AbstractController
{

    /**
     * @Route("/question", name="question")
     */
    public function index(Request $request,TranslatorInterface $translator): Response
    {   
        $session = $request->getSession();
        $cookies = $request->cookies;

        if ($session){
        }else{
            $session = new Session();
            $session->start();
        }

        if ($request->query->get("button")){
            $session->set('num_reponse'.$session->get('num_question'), $request->query->get("button")); //$request->request->get("button")
        }

        if ($session->get('num_question')){
            if ($session->get('num_question')==20){
                $session->set('total_num_question',$session->get('num_question'));
                
                $score=0;
                for ($i=1;$i<=($session->get('num_question'));$i++) { 
                    $phrase = $session->get('question_complet'.$i)['bonne_reponse'];
                    $bonne_reponse = str_replace(' ', '_', $phrase);
                    if ($session->get("num_reponse".$i) == $bonne_reponse){
                        $score++;
                    } 
                }

                if ($cookies->get('chrono')){
                    $point=$score*10;

                    $testCookies=$cookies->get('chrono');
                    $now=date("H:i:s");
                    $time=explode(' ', $testCookies);
                    $time2=explode(':', $time[4]);
                    $now2=explode(':', $now);

                    $timeh=$now2[0]-$time2[0];
                    $timem=$now2[1]-$time2[1];
                    $times=$now2[2]-$time2[2];

                    $seconde=($timeh*3600)+($timem*60)+$times;
                    $multiplicateur=round(120/$seconde, 2);
                        
                    $pointTotal=$point*$multiplicateur;

                    $repository=$this->getDoctrine()->getRepository(User::class);
                    $test=$this->getUser()->getId();
                    $userAct=$repository->findOneby(array('id'=>$test));
                    $userAct->setLastscore($pointTotal);
                    $userBestScore=$userAct->getBestscore();
                    if ($userBestScore<$pointTotal){
                        $userAct->setBestscore($pointTotal);
                    }
                
                    $em=$this->getDoctrine()->getManager();
                    $em->persist($userAct);
                    $em->flush();
                }

                $session->set('num_question', 0);

                if($cookies->get('chrono')){
                    return $this->render('question/reponse.html.twig', [
                        'controller_name' => 'QuestionController',
                        'testscore' => $score,
                        'testpoint' => $point,
                        'testpointTotal' => $pointTotal,
                        'testmultiplicateur' => $multiplicateur,
                    ]);
                    $sessionEnd = $request->getSession();
                    $sessionEnd->invalidate();  
                }else{
                    return $this->render('question/reponse.html.twig', [
                        'controller_name' => 'QuestionController',
                        'testscore' => $score,
                    ]);
                    $sessionEnd = $request->getSession();
                    $sessionEnd->invalidate(); 
                }
                
            }else{                          
                $newvar=$session->get('num_question');
                $newvar++;
                $session->set('num_question',$newvar);
            }
        }else{                                  
            $session->set('num_question', 1);
        }

        $typeQuestion=random_int(1,2);

        if ($typeQuestion==1){
            return $this->redirectToRoute('question_type_a', $request->query->all());
        }else{
            return $this->redirectToRoute('question_type_b', $request->query->all());
        }
    }

    /**
     * @Route("/question/question_type_a", name="question_type_a")
     */
    function question_type_a(Request $request,TranslatorInterface $translator) {

        $locale = $request->getLocale();
        if($locale){
            switch($locale){ // selectionne le tableau selon la langue
                default:
                case 'fr':
                    $em=$this->getDoctrine()->getRepository(PlantFr::class);
                    break;
                case 'en':
                    $em=$this->getDoctrine()->getRepository(PlantEn::class);
                    break;
            } 
        }else{
            $em=$this->getDoctrine()->getRepository(PlantFr::class);
        }

        $session = $request->getSession();
        $AllPlant=$em->findAll();
        $count_tableau=count($AllPlant);
        $liste_id=array(); // créer une liste de tout les mauvaise image/réponse
        for ($i=0;$i<$count_tableau;$i++) { 
            $liste_id[]=$AllPlant[$i]->getId();
        }

        $type="typeA";
        $nombre=random_int(0,$count_tableau-1); //nombre entre 0 et x , definit la ligne pour bonne image
        $categorie=random_int(1,8); //nombre entre 1 et 8 , definit la colonnes
        $categorie_type="Description";
        switch ($categorie){
            case 2:
                $categorie_type="NativeFrom";
                break;
            case 3:
                $categorie_type="Type";
                $categorie_type_trad="Type";
                break;
            case 4:
                $categorie_type="Genus";
                $categorie_type_trad="Genus";
                break;
            case 5:
                $categorie_type="Family";
                $categorie_type_trad="Family";
                break;
            case 6:
                $categorie_type="CommonUse";
                break;
            case 7: 
                $categorie_type="FlowerinIn";
                break;
            case 8:
                $categorie_type="PlantType";
                break;
        }
        $nombre_image=random_int(1,2); //nombre entre 1 et 2 , selectionne une image 

        $plantbyid=$em->findOneBy(array('id' => $liste_id[$nombre])); //va chercher dans le tableau le nom latin
        $bon_nom_image=$plantbyid->getLatinName();

        if($locale){
            switch($locale){ // selectionne le tableau selon la langue
                default:
                case 'fr':
                    $reflectionMethod = new ReflectionMethod('App\Entity\PlantFr','get'.$categorie_type);
                    break;
                case 'en':
                    $reflectionMethod = new ReflectionMethod('App\Entity\PlantEn','get'.$categorie_type);
                    break;
            } 
        }else{
            $reflectionMethod = new ReflectionMethod('App\Entity\PlantFr','get'.$categorie_type);
        }

        $bon_categorie_image = $reflectionMethod->invoke($plantbyid);

        $bonne_reponse=$bon_nom_image.$nombre_image.".jpg"; //donne le nom d'une plante + un nombre / ex : Adansonia_digitata2.png

        //selectionne les 3 mauvaise images

        $liste_mauvaise_image=array(); // créer une liste de tout les mauvaise image/réponse
        for ($i=1;$i<$count_tableau;$i++) { 
            $plant_a_tester=$em->findOneBy(array('id' => $liste_id[$i]));

            $categorie_a_tester= $reflectionMethod->invoke($plant_a_tester);

            if ($categorie_a_tester!=$bon_categorie_image){
                $liste_mauvaise_image[]=$plant_a_tester->getLatinName();
            }
        }

        $mauvais1=random_int(0,(count($liste_mauvaise_image)-1)); //nombre entre 0 et x , definit la ligne pour mauvaise image 1
        $mauvais2=$mauvais1; 
        $mauvais3=$mauvais1; 
        
        while ($mauvais2==$mauvais1) {
            $mauvais2=random_int(0,(count($liste_mauvaise_image)-1)); //nombre entre 0 et x , definit la ligne pour mauvaise image 2
        }
        while ($mauvais3==$mauvais1||$mauvais3==$mauvais2) {
            $mauvais3=random_int(0,(count($liste_mauvaise_image)-1)); //nombre entre 0 et x , definit la ligne pour mauvaise image 3
        }

        for ($i=1;$i<4;$i++) { 
            ${"nombre_image".($i+1)}=random_int(1,2); 
            ${"mauvaise_reponse".$i}=($liste_mauvaise_image[${"mauvais".$i}]).${"nombre_image".($i+1)}.".jpg"; //sort le nom d'une plante + un nombre / ex : Adansonia_digitata2.png	
        }

        switch ($categorie){
            case 1:
                $question_text=$translator->trans('question.description')." ".$bon_categorie_image; 
                break;
            case 2:
                $question_text=$translator->trans('question.natif')." ".$bon_categorie_image." ?"; 
                break;
            case 8:
                $question_text=$translator->trans('question.vegetal_est')." ".$bon_categorie_image." ?"; 
                break;
            case 3:
            case 4:
            case 5:
                $question_text=$translator->trans('question.vegetal_est_de')." ".$categorie_type_trad." : ".$bon_categorie_image." ?"; 
                break;
            case 6:
                $question_text=$translator->trans('question.se_trouve_principalement')." ".$bon_categorie_image." ?"; 
                break;
            case 7:
                if ($bon_categorie_image!=$translator->trans('question.pas_de_fleur')) {
                    $question_text=$translator->trans('question.plante_éclos_en')." ".$bon_categorie_image." ?"; 
                    break;
                }else{
                    $question_text=$translator->trans('question.plante_n_a_pas')." ";
                    break;
                }  
        }
        $reponses=array($bonne_reponse,$mauvaise_reponse1,$mauvaise_reponse2,$mauvaise_reponse3);
        $reponse1=array_rand($reponses);
        $reponse2=$reponse1;
        $reponse3=$reponse1;
        $reponse4=$reponse1;

        while ($reponse2==$reponse1){
            $reponse2=array_rand($reponses);
        }
        while ($reponse3==$reponse1||$reponse3==$reponse2){
            $reponse3=array_rand($reponses);
        }
        while ($reponse4==$reponse1||$reponse4==$reponse2||$reponse4==$reponse3){
            $reponse4=array_rand($reponses);
        }

        $name_question='question_complet'.$session->get('num_question');
        $session->set($name_question, array(
            'type' => $type,
            'question_text' => $question_text,
            'bonne_reponse' => $bonne_reponse,
            'ordre1' => $reponses[$reponse1], 
            'ordre2' => $reponses[$reponse2],
            'ordre3' => $reponses[$reponse3],
            'ordre4' => $reponses[$reponse4],
            'categorie_type' => $categorie_type,
            'num_question' => $session->get('num_question'),
        ));

        return $this->render('question/question.html.twig', [
            'type' => $type,
            'question_text' => $question_text,
            'bonne_reponse' => $bonne_reponse,
            'ordre1' => $reponses[$reponse1], 
            'ordre2' => $reponses[$reponse2],
            'ordre3' => $reponses[$reponse3],
            'ordre4' => $reponses[$reponse4],
            'categorie_type' => $categorie_type,
        ]);
    }

    /**
     * @Route("/question/question_type_b", name="question_type_b")
     */
    function question_type_b(Request $request,TranslatorInterface $translator) {

        $locale = $request->getLocale();
        if($locale){
            switch($locale){ // selectionne le tableau selon la langue
                default:
                case 'fr':
                    $em=$this->getDoctrine()->getRepository(PlantFr::class);
                    break;
                case 'en':
                    $em=$this->getDoctrine()->getRepository(PlantEn::class);
                    break;
            } 
        }else{
            $em=$this->getDoctrine()->getRepository(PlantFr::class);
        }

        $session = $request->getSession();
        $AllPlant=$em->findAll();
        $count_tableau=count($AllPlant);
        $liste_id=array(); // créer une liste de tout les mauvaise image/réponse
        for ($i=0;$i<$count_tableau;$i++) { 
            $liste_id[]=$AllPlant[$i]->getId();
        }
        
        $nombre=random_int(0,$count_tableau-1); //nombre entre 0 et x , definit la ligne pour bonne image
        $plantbyid=$em->findOneBy(array('id' => $liste_id[$nombre])); //selectionne la bonne ligne
        $bon_Latin=$plantbyid->getLatinName(); //va chercher dans le tableau le nom latin
        $bon_nom=$plantbyid->getName(); //va chercher dans le tableau le nom

        $soustype=random_int(1,4); // définit si la question est le nom latin, le nom commun ou une image
        if ($soustype==3||$soustype==4){
            $nombre_image=random_int(1,2); //nombre entre 1 et 2 , selectionne une image 
            $bonne_image=$bon_Latin.$nombre_image.".jpg"; //donne le nom d'une plante + un nombre / ex : Adansonia_digitata2.png
        }

        //selectionne les 3 mauvaise image

        $mauvais1=$nombre;
        $mauvais2=$nombre;
        $mauvais3=$nombre; 
        
        while ($mauvais1==$nombre) {
            $mauvais1=random_int(0,$count_tableau-1); //nombre entre 0 et x , definit la ligne pour mauvaise image 1
        }
        while ($mauvais2==$nombre||$mauvais2==$mauvais1) {
            $mauvais2=random_int(0,$count_tableau-1); //nombre entre 0 et x , definit la ligne pour mauvaise image 2
        }
        while ($mauvais3==$nombre||$mauvais3==$mauvais1||$mauvais3==$mauvais2) {
            $mauvais3=random_int(0,$count_tableau-1); //nombre entre 0 et x , definit la ligne pour mauvaise image 3
        }

        if ($soustype==1||$soustype==3){
            for ($i=1;$i<4;$i++) { 
                $mauvaise_plantbyid=$em->findOneBy(array('id' => $liste_id[${"mauvais".$i}])); //selectionne la bonne ligne
                ${"mauvaise_reponse".$i}=$mauvaise_plantbyid->getName(); //sort le nom commun de chaque mauvaise réponce
            }
        }else {
            for ($i=1;$i<4;$i++) { 
                $mauvaise_plantbyid=$em->findOneBy(array('id' => $liste_id[${"mauvais".$i}])); //selectionne la bonne ligne
                ${"mauvaise_reponse".$i}=$mauvaise_plantbyid->getLatinName(); //sort le nom latin de chaque mauvaise réponce
            }
        }

        switch($soustype){ // selectionne la phrase de la question selon le sous type
            default:
            case 1:
                $question_text=$translator->trans('question.nom_commun1')." ".$bon_Latin." ?";
                $bonne_reponse=$bon_nom;
                $question_image="";
                $type="typeB"; 
                break;
            case 2:
                $question_text=$translator->trans('question.nom_latin1')." ".$bon_nom." ?"; 
                $bonne_reponse=$bon_Latin;
                $question_image="";
                $type="typeB";
                break;
            case 3:
                $question_text=$translator->trans('question.nom_commun2')." ";
                $question_image=$bonne_image; 
                $bonne_reponse=$bon_nom;
                $type="typeC";
                break;
            case 4:    
                $question_text=$translator->trans('question.nom_latin2')." ";
                $question_image=$bonne_image; 
                $bonne_reponse=$bon_Latin;
                $type="typeC";
                break;
        }
        $reponses=array($bonne_reponse,$mauvaise_reponse1,$mauvaise_reponse2,$mauvaise_reponse3);
        $reponse1=array_rand($reponses);
        $reponse2=$reponse1;
        $reponse3=$reponse1;
        $reponse4=$reponse1;

        while ($reponse2==$reponse1){
            $reponse2=array_rand($reponses);
        }
        while ($reponse3==$reponse1||$reponse3==$reponse2){
            $reponse3=array_rand($reponses);
        }
        while ($reponse4==$reponse1||$reponse4==$reponse2||$reponse4==$reponse3){
            $reponse4=array_rand($reponses);
        }

        $name_question='question_complet'.$session->get('num_question');
        $session->set($name_question, array(
            'type' => $type,
            'question_text' => $question_text,
            'question_image' => $question_image,
            'bonne_reponse' => $bonne_reponse,
            'ordre1' => $reponses[$reponse1], 
            'ordre2' => $reponses[$reponse2],
            'ordre3' => $reponses[$reponse3],
            'ordre4' => $reponses[$reponse4],
            'num_question' => $session->get('num_question'),
        ));

        return $this->render('question/question.html.twig', [
            'type' => $type,
            'question_text' => $question_text,
            'question_image' => $question_image,
            'bonne_reponse' => $bonne_reponse,
            'ordre1' => $reponses[$reponse1], 
            'ordre2' => $reponses[$reponse2],
            'ordre3' => $reponses[$reponse3],
            'ordre4' => $reponses[$reponse4],
        ]);
    }
}
