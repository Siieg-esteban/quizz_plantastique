<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use App\Entity\User;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
    /**
    * @Route("/forgotten-password", name="app_forgotten_password",methods="GET|POST")
    */
    public function forgottenPassword(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Check'])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(array('email' => $form['email']->getData()));
            if ($user) {
                $config=parse_url($_ENV['MAILER_URL']);

                // Create the Transport
                $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465,'ssl'))          
                ->setUsername($config["user"])
                ->setPassword($config["pass"])
                ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
            
                // Create the Mailer using your created Transport
                $mailer = new Swift_Mailer($transport);

                // Create a message
                $message = (new Swift_Message('Mot de passe perdu'))
                    ->setFrom(['no-reply-plantastique' => 'Planti bot'])
                    ->setTo([$form->get('email')->getData()])
                    ->setBody($this->renderView('mails/forgottenpassword.html.twig', [
                        'name' => $form->get('email')->getData(),
                        'token' => $user->getResetToken()
                    ]),'text/html');
                                
                // Send the message
                $result = $mailer->send($message);
                $this->addFlash(
                    'notice',
                    'Email envoyé. Vérifiez votre messagerie'
                );
                return $this->render('security/forgottenpassword.html.twig', [
                    'form' => null
                ]);     
            }
            else {
                $this->addFlash(
                'error',
                'Email non trouvé'
                );
            }
        }
        return $this->render('security/forgottenpassword.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /** Réinitialiation du mot de passe par mail
    * @Route("/change-password/{token}", name="app_reset_password")
    */
    public function resetPassword(Request $request, string $token,UserPasswordEncoderInterface $passwordEncoder) : Response {
        $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->findOneBy(array('resetToken' => $token)); 
        if ($user) {
            $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
            ->add('save', SubmitType::class, ['label' => 'Change'])
            ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword($passwordEncoder->encodePassword($user,
                $form->get('password')->getData()));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                // do anything else you need here, like send an email
                $config=parse_url($_ENV['MAILER_URL']);

                // Create the Transport
                $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465,'ssl'))
                ->setUsername($config["user"])
                ->setPassword($config["pass"])
                ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
        
                // Create the Mailer using your created Transport
                $mailer = new Swift_Mailer($transport);

                // Create a message
                $message = (new Swift_Message('Notification de changement de mot de passe'))
                ->setFrom(['quizzplantastique2020@gmail.com' => 'Planti bot'])
                ->setTo([$user->getEmail()])
                ->setBody($this->renderView('mails/resetpassword.html.twig', [
                    'name' => $user->getEmail(),
                ]),'text/html');
                
                // Send the message
                $result = $mailer->send($message);
                $this->addFlash(
                'notice',
                'Mot de passe changé. Veuillez vous connecter.'
                );
                return $this->redirectToRoute('app_login');
            }
            return $this->render('security/resetpassword.html.twig', [
                'form' => $form->createView()
            ]);
        } 
        else {
            $this->addFlash(
            'error',
            'Utilisateur non trouvé'
            );
        }
        return $this->render('security/resetpassword.html.twig', [
            'form' => null
        ]);
    }
}