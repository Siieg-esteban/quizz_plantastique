<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Form\PlantSelect;
use App\Form\PlantType;
use App\Entity\PlantFr;
use App\Entity\PlantEn;
use App\Entity\Comment;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

class PlantastiqueController extends AbstractController
{
    /**
     * @Route("/", name="plantastique")
     */
    public function index(Request $request): Response
    {
        $session = $request->getSession();
        if ($session->get('num_question')!=0){
            $session->set('num_question', 0);
        }

        $em=$this->getDoctrine()->getRepository(Comment::class);
        $userClass=$this->getDoctrine()->getRepository(User::class);
        $AllComment=$em->findBy(array(),array('postdate' => 'desc'));
        $AllUser=$userClass->findBy(array(),array('bestscore' => 'desc'));

        $comment=array();
        $scoreUser=array();
        for ($i=0;$i<5;$i++) { 
            $comment[]=$AllComment[$i];
            $scoreUser[]=$AllUser[$i];
        }

        return $this->render("site/index.html.twig",[
            'comment' => $comment,
            'scoreUser' => $scoreUser,
        ]);
    }

    /**
     * @Route("/error", name="plantaerror")
     */
    public function error(TranslatorInterface $translator): Response
    {
        $this->addflash(
            'notice',
            $translator->trans('flash.plantaerror')
        );
        return $this->render("site/index.html.twig");
    }
    /**
     * @Route("/plant", name="select")
     */
    public function selection(Request $request): Response
    {
        $select= $this->createForm(PlantSelect::class);
        $select->handleRequest($request);
        if($select->isSubmitted() and $select->isValid()){
            $lang=$select['lang']->getData(); 
            return $this->redirectToRoute('plant_listing', ['lang'=>$lang]);
        }
        return $this->render('plant/plantrouting.html.twig', [
            'select'=> $select->createView(),
        ]);
    } 

    /**
     * @Route("/plant/listing/{lang}", name="plant_listing")
     */
    public function listing($lang): Response
    {
        switch($lang){
            case "fr":// si la modification du végétal est en français
                $repository=$this->getDoctrine()->getRepository(PlantFr::class);
            break;
            case "en":// si la modification du végétal est en anglais
                $repository=$this->getDoctrine()->getRepository(PlantEn::class);
            break;
        }
        $listplant = $repository->findall();
        return $this->render("plant/plantlisting.html.twig",['listplant'=>$listplant, 'lang'=>$lang]);
    }
    /**
     * @Route("/plant/Update/{id}/{lang}", name="update")
     */
    public function Update($id, $lang,Request $request, TranslatorInterface $translator): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(PlantType::class, array());
        $form->handleRequest($request);

        switch($lang){
            case "fr":// si la modification du végétal est en français
                $repository=$this->getDoctrine()->getRepository(PlantFr::class);
            break;
            case "en":// si la modification du végétal est en anglais
                $repository=$this->getDoctrine()->getRepository(PlantEn::class);
            break;
        }
        $plant=$repository->findOneby(array('id'=>$id));
        
        if($form->isSubmitted() and $form->isValid()){
            $plant->setName($form['name']->getData());
            $plant->setLatinName($form['latinname']->getData());
            $plant->setDescription($form['description']->getData());
            $plant->setNativeFrom($form['native']->getData());
            $plant->setType($form['type']->getData());
            $plant->setGenus($form['genus']->getData());
            $plant->setFamily($form['family']->getData());
            $plant->setLeafColor($form['color']->getData());
            $plant->setCommonUse($form['use']->getData());
            $plant->setFlowerinIN($form['flower']->getData());
            $plant->setPlantType($form['planttype']->getData());

            $em=$this->getDoctrine()->getManager();
            $em->persist($plant);
            $em->flush();
            $this->addflash(
                'notice',
                $translator->trans('flash.update')
            );
            return $this->redirectToRoute('plant_listing',['lang'=>$lang]);
        }    
        return $this->render('plant/plantupdate.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/plant/create/{lang}", name="plant_create")
     */
    public function plantcreate($lang, Request $request, TranslatorInterface $translator): Response
    {
        switch($lang){
            case "fr":// si la modification du végétal est en français
                $plant = new PlantFr(); 
            break;
            case "en":// si la modification du végétal est en anglais
                $plant = new PlantEn();
            break;
        }
        $form=$this->createForm(PlantType::class, array());
        $form->handlerequest($request);

        if($form->isSubmitted() and $form->isValid()){
            $plant->setName($form['name']->getData());
            $plant->setLatinName($form['latinname']->getData());
            $plant->setDescription($form['description']->getData());
            $plant->setNativeFrom($form['native']->getData());
            $plant->setType($form['type']->getData());
            $plant->setGenus($form['genus']->getData());
            $plant->setFamily($form['family']->getData());
            $plant->setLeafColor($form['color']->getData());
            $plant->setCommonUse($form['use']->getData());
            $plant->setFlowerinIN($form['flower']->getData());
            $plant->setPlantType($form['planttype']->getData());
            $em=$this->getDoctrine()->getManager();
            $em->persist($plant);
            $em->flush();
            $this->addflash(
                'notice',
                $translator->trans('flash.create')
            );
            //images upload
            /** @var UploadedFile $uploadedFile */
            $uploadedFile =$form['image1']->getData();
            /** @var UploadedFile $uploadedFile2 */
            $uploadedFile2 =$form['image2']->getData();
            $destination= $this->getParameter('kernel.project_dir').'/public/image/image_plant';
            $image1= $form['latinname']->getData()."1".".jpg";
            $uploadedFile->move($destination, $image1);
            $image2= $form['latinname']->getData()."2".".jpg";
            $uploadedFile2->move($destination, $image2);
            
            return $this->redirectToRoute('plant_listing',['lang'=>$lang]);
        }    
        return $this->render('plant/plantupdate.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/plant/Delete/{id}/{lang}", name="delete")
     */
    public function delete($id, $lang, Request $request, TranslatorInterface $translator): Response
    {
        switch($lang){
            case "fr":// si la modification du végétal est en français
                $repository=$this->getDoctrine()->getRepository(PlantFr::class);
            break;
            case "en":// si la modification du végétal est en anglais
                $repository=$this->getDoctrine()->getRepository(PlantEn::class);
            break;
        }
        $plant=$repository->findOneby(array('id'=>$id));

        $em=$this->getDoctrine()->getManager();
        $em->remove($plant);
        $em->flush();
        $this->addFlash(
            'notice',
            $translator->trans('flash.del')
        );
        return $this->redirectToRoute('plant_listing',['lang'=>$lang]);
    }

    /**
     * @Route("/account", name="account")
     */
    public function account(Request $request, TranslatorInterface $translator): Response
    {
        // comments
        $em=$this->getDoctrine()->getRepository(Comment::class);
        $userClass=$this->getDoctrine()->getRepository(User::class);
        $userid=$this->getUser()->getId();
        $AllComment=$em->findBy(array(),array('postdate' => 'desc'));
        $AllUser=$userClass->findBy(array(),array('bestscore' => 'desc'));
        $comment=$em->findBy(array('user_id' => $userid));
        $countuser=count($AllUser);
        // ranking user
        for ($y=0;$y<$countuser;$y++){
            if ($AllUser[$y]->getId()==$userid){
                $rank=$y+1;
            }
        }
        return $this->render('site/account.html.twig',[
            'comment' => $comment,
            'rank' => $rank,
            
        ]);
    }

    /**
     * @Route("/comment", name="comment")
     */
    public function comment(Request $request,TranslatorInterface $translator): Response
    {
        if ($request->query->get("textComment")){
            $em=$this->getDoctrine()->getRepository(User::class);
            $test=$this->getUser()->getId();
            $userid=$em->findOneBy(array('id' => $test));

            $Comment = new Comment();
            $date = new \DateTime('@'.strtotime('now'));

            $Comment->setCommentary($request->query->get("textComment"));
            $Comment->setPostdate($date);
            $Comment->setUserId($userid);

            $em=$this->getDoctrine()->getManager();
            $em->persist($Comment);
            $em->flush();
        }

        return $this->redirectToRoute('plantastique');
    }
}
