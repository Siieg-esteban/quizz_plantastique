-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 11 jan. 2021 à 15:00
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `plantastique`
--

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `lastscore`, `bestscore`, `lastchrono`, `bestchrono`, `signindate`, `avatar`) VALUES
(1, 'Andrew', 'Begum', 'Andrew.Begum@mail.com', '1234', 1, 1, NULL, NULL, '1645-12-17', NULL),
(2, 'Elliot', 'Griffin', 'Elliot.Griffin@mail.com', '1234', 12, 14, NULL, NULL, '1978-01-24', NULL),
(3, 'Georgia', 'Austin', 'Georgia.Austin@mail.com', '1234', 17, 18, NULL, NULL, '2014-07-02', NULL),
(4, 'Eva', 'Dean', 'Eva.Dean@mail.com', '1234', 13, 15, NULL, NULL, '2020-09-08', NULL),
(5, 'Scarlett', 'Conway', 'Scarlett.Conway@mail.com', '1234', 8, 8, NULL, NULL, '2020-07-27', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
