-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 11 jan. 2021 à 15:01
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `plantastique`
--

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `commentary`, `postdate`, `user_id`) VALUES
(1, 'c pa bi1', '2021-01-05 10:12:45', 3),
(2, 'Ego vero sic intellego, Patres conscripti, nos hoc tempore in provinciis decernendis perpetuae', '2003-04-17 05:32:45', 2),
(3, 'Très bien, mais Eric Zemmour a quand même essayé de rendre publiques des idées que les Français n\'osent pas exprimer, dans la mesure où dans la mesure du possible on évitera de parler sans savoir juste pour rabaisser le milieu hospitalier (sauf si on fait disparaitre les chats d\'internet) , et qui n\'est pas d\'accord pour dire que Anne Hidalgo et son entourage ont très peu d\'arguments, finalement . Sans parler des lobby en tous genres, à aucun moment les dirigeants des GAFA qui promettent toujours la Lune sans noyer le poisson . On vit vraiment dans une époque de fous. ', '2018-05-15 18:18:53', 5);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
